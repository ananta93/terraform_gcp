#############################################################
#                                                           #
#                      Project Credentials                  #
#                                                           #
#############################################################
variable "GOOGLE_APPLICATION_CREDENTIALS" {}

#############################################################
#                                                           #
#                     TF state file bucket                  #
#                                                           #
#############################################################
terraform {
  backend "gcs" {
    bucket = "new-terraform-tfstate"
    prefix = "terraform"
  }
}

#############################################################
#                                                           #
#                            Provider                       #
#                                                           #
#############################################################
provider "google" {
  credentials = var.GOOGLE_APPLICATION_CREDENTIALS
  project     = "ananta-demo"
  region      = "us-west1"
}

#############################################################
#                                                           #
#                     Local variables                       #
#                                                           #
#############################################################
locals {
  project = "ananta-demo"
  region  = "us-west1"
}


#############################################################
#                                                           #
#                         Enable APIs                       #
#                                                           #
#############################################################

resource "google_project_service" "enable_apis" {
  project = local.project
  service = "cloudresourcemanager.googleapis.com"
}

#############################################################
#                                                           #
#             IAM members and Policy assign                 #
#                                                           #
#############################################################

resource "google_project_iam_member" "iam_member" {
  project = local.project
  role    = "roles/editor"
  member  = "anantadas.dilan@gmail.com"
}


#############################################################
#                                                           #
#                        Resources                          #
#                                                           #
#############################################################

######################### Storage Bucket ####################

# module "storage_bucket_01" {
#   # source = "./modules/storage-bucket"
#   source                      = "git::https://github.com/ananta93/gcp-terraform-modules.git//modules/storage-bucket?ref=main"
#   bucket_name                 = "ananta-demo-bucket-02"
#   location                    = "US-WEST1"
#   force_destroy               = true
#   project                     = local.project
#   storage_class               = "STANDARD"
#   uniform_bucket_level_access = true
# }
